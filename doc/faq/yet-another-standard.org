★ [[doc/FAQs.org][← FAQs overview page]] ★

* Why do we need yet another standard?

It's an old IT tradition to create new standards.

[[https://imgs.xkcd.com/comics/standards_2x.png]]

(Source: [[https://xkcd.com/927/]])

As of 2021-08, [[../Org-mode.org][Org-mode]] itself lacks a unified specification. To gap
this bridge and to provide a viable sub-set of syntax elements for
third party tool support, Orgdown and Orgdown1 was initially defined by
[[https://Karl-Voit.at][Karl Voit]] in mid 2021. 

Technically, Orgdown is a well-defined subset of Org-mode and may
therefore be not be considered as a completely new standard, more of a
format specification of an existing thing.

Orgdown helps people when it comes to Org-mode syntax element support
in non-Emacs tools. With a [[../Tool-Support.org][tool support]] of hundred percent for
Orgdown1, anybody is able to tell what syntax elements are supposed to
work with that tool. If a specific tool has less than hundred percent,
you need to take a look at the tool-specific sub-pages of [[../Tool-Support.org][the
tool-support page]] for further details.

You can read more about the motivation [[https://karl-voit.at/2021/11/27/orgdown/][on this blog article]].

---------------

★ [[doc/FAQs.org][← FAQs overview page]] ★
