★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for GitHub

If you put text files on GitHub whose file extension is =.org=, it
renders its content using [[https://github.com/wallyqs/org-ruby][org-ruby]] in the browser just like this page
by GitLab.

- Platform: Web
- License: The MIT License for org-ruby
- Price: [[https://github.com/pricing][Freemium]] for GitHub
- [[https://github.com/novoid/github-orgmode-tests/blob/master/orgmode_support.org][Example]] 
- [[https://github.com][Homepage]]
- last Orgdown1 syntax assessment: 2021-09-16 by [[https://Karl-Voit.at][Karl Voit]] 

| *Percentage of Orgdown1 syntax support:* | 95 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment |
|-----------------------------------------------------------------------------+-----+-------------|
| syntax elements do not need to be separated via empty lines                 |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| *bold*                                                                      |   2 |             |
| /italic/                                                                    |   2 |             |
| _underline_                                                                 |   2 |             |
| +strike through+                                                            |   2 |             |
| =code=                                                                      |   2 |             |
| ~commands~                                                                  |   2 |             |
| Combinations like *bold and /italic/ text*                                  |   1 |             |
| : example                                                                   |   2 |             |
| [[https://example.com][link *bold* test]]                                                            |   2 |             |
| [[https://example.com][link /italic/ test]]                                                          |   2 |             |
| [[https://example.com][link +strike through+ test]]                                                  |   2 |             |
| [[https://example.com][link =code= test]]                                                            |   2 |             |
| [[https://example.com][link ~commands~ test]]                                                        |   2 |             |
| ≥5 dashes = horizontal bar                                                  |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Headings using asterisks                                                    |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Unordered list item with "-"                                                |   2 |             |
| Ordered list item with 1., 2., …                                            |   2 |             |
| Checkbox + unordered list item with "-"                                     |   1 |             |
| Checkbox + ordered list item with 1., 2., …                                 |   1 |             |
| Mixed lists of ordered and unordered items                                  |   2 |             |
| Multi-line list items with collapse/expand                                  |   2 |             |
| Multi-line list items with support for (auto-)indentation                   |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Example block                                                               |   2 |             |
| Quote block                                                                 |   2 |             |
| Verse block                                                                 |   2 |             |
| Src block                                                                   |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Comment lines with "# <foobar>"                                             |   2 |             |
| Comment block                                                               |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |   2 |             |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |   2 |             |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Basic table functionality                                                   |   2 |             |
| Table cells with *bold*                                                     |   2 |             |
| Table cells with /italic/                                                   |   2 |             |
| Table cells with _underline_                                                |   2 |             |
| Table cells with +strike through+                                           |   2 |             |
| Table cells with =code=                                                     |   2 |             |
| Table cells with ~commands~                                                 |   2 |             |
| Auto left-aligning of text                                                  |   2 |             |
| Auto right-aligning of numbers like "42.23"                                 |   2 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| Other syntax elements are interpreted at least as normal text (or better)   |   2 |             |
| Other syntax elements: linebreaks respected (or better)                     |   1 |             |
|-----------------------------------------------------------------------------+-----+-------------|
| *Sum*                                                                       |  82 |             |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)



